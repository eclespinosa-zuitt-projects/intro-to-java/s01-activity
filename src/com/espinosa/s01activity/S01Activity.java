package com.espinosa.s01activity;

public class S01Activity {
    public static void main(String[] args) {
        boolean result = true;
        char capitalLetter = 'E';
        int birthYear = 1964;
        float randomNumber = 3.655f;

        System.out.println("The result is " + result + ".\nCapital Letter e is " + capitalLetter +
                ".\nBirthYear is " + birthYear + ".\nThe random number is " + randomNumber + ".");


    }
}
